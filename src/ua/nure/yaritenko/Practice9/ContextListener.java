package ua.nure.yaritenko.Practice9;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;
import java.util.ArrayList;
import java.util.List;

public class ContextListener implements ServletContextListener  {


    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        List<String> listGames = new ArrayList<>();
        listGames.add("Football");
        listGames.add("Biathlon");
        listGames.add("Basketball");
        ServletContext sc = servletContextEvent.getServletContext();
        sc.setAttribute("listGames", listGames);
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {

    }
}
