package ua.nure.yaritenko.Practice9;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;


@WebServlet("/Calc")
public class Calc extends HttpServlet {
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        int x, y;
        String operation;
        x = Integer.parseInt(request.getParameter("x"));
        y = Integer.parseInt(request.getParameter("y"));
        System.out.println(y);
        operation = request.getParameter("op");
        response.setContentType("text/html; charset=utf-8");
        PrintWriter out = response.getWriter();
        if (operation.equals("minus")) {
            out.print("<html><body>");
            out.print(x - y);
            out.print("</body></html>");
        }
    }
}

