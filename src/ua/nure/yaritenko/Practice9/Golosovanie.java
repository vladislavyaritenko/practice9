package ua.nure.yaritenko.Practice9;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;

@WebServlet("/Golosovanie")
public class Golosovanie extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html; charset=utf-8");
        HttpSession httpSession = req.getSession();
        ServletContext sc = req.getServletContext();
        String game = req.getParameter("games");
        String name = req.getParameter("name");
        List<String> namesInSession = null;
        if (httpSession.getAttribute("namesInSession") == null) {
            namesInSession = new ArrayList<>();
        }
        if (httpSession.getAttribute("namesInSession") != null) {
            namesInSession = (List<String>) httpSession.getAttribute("namesInSession");
        }
        if (namesInSession.contains(name)) {
            req.setAttribute("Stop", "stop");
        } else {
            namesInSession.add(name);
            httpSession.setAttribute("namesInSession", namesInSession);
            int count = 0;
            List<String> listFootball = null;
            if (sc.getAttribute("listFootball") == null) {
                listFootball = new ArrayList<>();
            }
            List<String> listBiathlon = null;
            if (sc.getAttribute("listBiathlon") == null) {
                listBiathlon = new ArrayList<>();
            }
            List<String> listBasketball = null;
            if (sc.getAttribute("listBasketball") == null) {
                listBasketball = new ArrayList<>();
            }
            Map<String, Integer> result = null;
            if (result == null) {
                result = new TreeMap<>();
                result.put("Football", count);
                result.put("Biathlon", count);
                result.put("Basketball", count);
            }
            if (sc.getAttribute("map") != null) {
                result = (Map<String, Integer>) sc.getAttribute("map");
            }
            count = result.get(game);
            result.put(game, ++count);
            if (sc.getAttribute("listFootball") != null) {
                listFootball = (List<String>) sc.getAttribute("listFootball");
            }
            if (sc.getAttribute("listBiathlon") != null) {
                listBiathlon = (List<String>) sc.getAttribute("listBiathlon");
            }
            if (sc.getAttribute("listBasketball") != null) {
                listBasketball = (List<String>) sc.getAttribute("listBasketball");
            }
            switch (game) {
                case "Football": {

                    listFootball.add(name);
                    break;
                }
                case "Biathlon": {

                    listBiathlon.add(name);
                    break;
                }
                case "Basketball": {

                    listBasketball.add(name);
                    break;
                }
            }
            sc.setAttribute("map", result);
            sc.setAttribute("listFootball", listFootball);
            sc.setAttribute("listBiathlon", listBiathlon);
            sc.setAttribute("listBasketball", listBasketball);
        }
        req.getRequestDispatcher("resultat.jsp").forward(req, resp);
    }
}
