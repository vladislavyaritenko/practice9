<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Resultat</title>
</head>
<body>
<c:if test="${Stop != null}">
    <h4>Вы уже голосовали!</h4>
</c:if>
<c:if test="${Stop == null}">
    <table border="1">
        <c:forEach items="${map}" var="item">
            <tr>
                <td>${item.key}</td>
                <td>${item.value}</td>
                <td>
                    <c:choose>
                        <c:when test="${item.key == 'Basketball'}">
                            <c:forEach items="${listBasketball}" var="names">
                                ${names}, &nbsp;
                            </c:forEach>
                        </c:when>
                        <c:when test="${item.key == 'Football'}">
                            <c:forEach items="${listFootball}" var="names">
                                ${names}, &nbsp;
                            </c:forEach>
                        </c:when>
                        <c:when test="${item.key == 'Biathlon'}">
                            <c:forEach items="${listBiathlon}" var="names">
                                ${names}, &nbsp;
                            </c:forEach>
                        </c:when>
                    </c:choose>
                </td>
            </tr>
        </c:forEach>
    </table>
</c:if>
</body>
</html>
