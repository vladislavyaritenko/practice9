<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <meta charset="utf-8">
    <title>Golosovanie</title>
</head>
<body>
<p><b>Голосование</b></p>
<form action="Golosovanie" method="get">
    <c:forEach items="${listGames}" var="item">
        <input type="radio" name="games" value="${item}" required>${item}<br>
    </c:forEach>
    <input type="text" name="name" required>Имя пользователя</input><br>
    <p><input type="submit" value="GO"></p>
</form>
</body>
</html>